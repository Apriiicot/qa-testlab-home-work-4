package pretashop3.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import pretashop3.utils.logging.EventHandler;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class BaseScript {

    protected EventFiringWebDriver driver;

    private WebDriver getDriver(String browser) {
        if(browser.equals(BrowserType.CHROME)){
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
            return new ChromeDriver();
        }else if(browser.equals(BrowserType.FIREFOX)) {
            System.setProperty(
                    "webdriver.gecko.driver",
                    new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
            return new FirefoxDriver();
        }else if(browser.equals(BrowserType.IEXPLORE)){
            System.setProperty(
                    "webdriver.ie.driver",
                    new File(BaseScript.class.getResource("/IEDriverServer.exe").getFile()).getPath());
            return new InternetExplorerDriver();
        }else {
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
            return new ChromeDriver();
        }
    }

    private EventFiringWebDriver getConfiguredDriver(String browser) {
        EventFiringWebDriver driver = new EventFiringWebDriver(getDriver(browser));
        driver.manage().timeouts()
                       .implicitlyWait(10, TimeUnit.SECONDS)
                       .pageLoadTimeout(15, TimeUnit.SECONDS) ;
        driver.register(new EventHandler());
        return driver;
    }

    @BeforeClass
    @Parameters("browser")
    public void setUp(@Optional String browser){
        driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }


}
