package pretashop3.tests;

import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pretashop3.model.ProductData;
import pretashop3.model.User;
import pretashop3.pages.AdminHomePage;
import pretashop3.pages.LoginPage;
import pretashop3.pages.SiteHomePage;

public class CreateProductTest extends BaseScript {

    private ProductData productData;

    @DataProvider(name = "Authentication")
    public Object[][] credentials(){
        return new Object[][]{{User.createValidUser()}};
    }

    @BeforeClass
    public void setUp(){
        this.productData = ProductData.generate();
    }

    @Test(dataProvider = "Authentication")
    public void createNewProduct(User user){
        new LoginPage(driver).loginUser(user)
                             .getSideBarElement()
                             .moveToProducts()
                             .createNewGoods()
                             .saveNewGoods(productData);
    }

    @Test(dependsOnMethods = "createNewProduct")
    public void checkCreateNewProduct(){
        new SiteHomePage(driver).moveToProductPage()
                                .checkAddingNewProduct(productData);

    }

}
