package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminProductsPage extends AbstractPageObject {

    public AdminProductsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//span[contains(text(),'Новый товар')]")
    private WebElement newGoodsButton;

    public NewProductPage createNewGoods(){
        waitForContentLoad(By.xpath("//span[contains(text(),'Новый товар')]"));
        newGoodsButton.click();
        return new NewProductPage(driver);
    }
}
