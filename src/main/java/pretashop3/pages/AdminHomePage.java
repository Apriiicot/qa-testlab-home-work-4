package pretashop3.pages;

import org.openqa.selenium.WebDriver;

public class AdminHomePage extends AbstractPageObject {

//    private static final String HOME_PAGE_URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";

    public AdminHomePage(WebDriver driver) {
        super(driver);
//        driver.navigate().to(HOME_PAGE_URL);
    }

    public SideBarElement getSideBarElement(){
        return new SideBarElement(driver);
    }
}
