package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class SideBarElement extends AbstractPageObject {

    public SideBarElement(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//span[contains(text(),'Каталог')]/..")
    private WebElement catalog;

    @FindBy(xpath = "//a[contains(text(),'категории')]")
    private WebElement category;

    public AdminProductsPage moveToProducts(){
        waitForContentLoad(By.xpath("//span[contains(text(),'Каталог')]"));
        Actions moveToCatalog = new Actions(driver);
        moveToCatalog.moveToElement(catalog).build().perform();
        catalog.click();
        return new AdminProductsPage(driver);
    }

}
