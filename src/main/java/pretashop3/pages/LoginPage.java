package pretashop3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pretashop3.model.User;

import java.util.concurrent.TimeUnit;

public class LoginPage extends AbstractPageObject {

    private static final String LOGIN_URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id= "passwd")
    private WebElement password;

    @FindBy(name = "submitLogin")
    private WebElement submitLogin;

    public AdminHomePage loginUser(User user){
        driver.navigate().to(LOGIN_URL);
        this.email.sendKeys(user.getEmail());
        this.password.sendKeys(user.getPassword());
        submitLogin.click();
        wait.withTimeout(10, TimeUnit.SECONDS);

        return new AdminHomePage(driver);
    }
}
