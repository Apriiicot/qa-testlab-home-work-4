package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPageObject {
    protected final WebDriver driver;
    protected final WebDriverWait wait;

    public AbstractPageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    public void waitForContentLoad(By loadElement) {
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(loadElement));
    }

    public void waitForContentVisible(By visibleElement){
        wait.until(ExpectedConditions.visibilityOfElementLocated(visibleElement));
    }

    public void quit(){
        this.driver.quit();
    }

    public void close(){
        this.driver.close();
    }
}
