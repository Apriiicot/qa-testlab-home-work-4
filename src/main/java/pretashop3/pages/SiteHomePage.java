package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SiteHomePage extends AbstractPageObject {

    private static final String HOME_PAGE_URL = "http://prestashop-automation.qatestlab.com.ua";

    public SiteHomePage(WebDriver driver) {
        super(driver);
        driver.navigate().to(HOME_PAGE_URL);
    }

    @FindBy(xpath = "//a[contains(text(),'Все товары')]")
    private WebElement allProductsButton;

    public SiteProductPage moveToProductPage(){
        waitForContentLoad(By.xpath("//a[contains(text(),'Все товары')]"));
        allProductsButton.click();
        return new SiteProductPage(driver);
    }
}
