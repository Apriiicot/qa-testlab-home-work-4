package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.asserts.SoftAssert;
import pretashop3.model.ProductData;

public class SiteProductPage extends AbstractPageObject {
    public SiteProductPage(WebDriver driver) {
        super(driver);
    }

    public void checkAddingNewProduct(ProductData productData){
        WebElement newProduct = driver.findElement(By.xpath(String.format("//a[contains(text(),'%s')]", productData.getName())));
        if(newProduct != null){
            newProduct.click();
            waitForContentLoad(By.className("h1"));
            SoftAssert productAssert = new SoftAssert();
            productAssert.assertEquals(driver.findElement(By.xpath(String.format("//span[contains(text(), '%s')]", productData.getName()))).getText(), productData.getName());
            productAssert.assertEquals(driver.findElement(By.xpath("//span[@itemprop='price']")).getAttribute("content"), productData.getPrice());
            productAssert.assertEquals(driver.findElement(By.xpath("//span[contains(text(), 'Товары')]")).getText().substring(0, 2).trim(), String.valueOf(productData.getQty()));
            productAssert.assertAll();
        }
    }
}
