package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pretashop3.model.ProductData;

public class NewProductPage extends AbstractPageObject{

    public NewProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "form_step1_name_1")
    private WebElement inputName;

    @FindBy(id = "form_step1_qty_0_shortcut")
    private WebElement inputCount;

    @FindBy(id = "form_step1_price_shortcut")
    private WebElement inputPrice;

    @FindBy(xpath = "//input[@id='form_step1_active']/..")
    private WebElement checkbocksOnlineOffline;

    @FindBy(xpath = "//span[contains(text(),'Сохранить')]")
    private WebElement saveProduct;

    public void saveNewGoods(ProductData productData){
        waitForContentLoad(By.id("form_step1_name_1"));
        inputName.sendKeys(productData.getName());
        waitForContentLoad(By.id("form_step1_qty_0_shortcut"));
        inputPrice.clear();
        inputPrice.sendKeys(Keys.chord(Keys.BACK_SPACE));
        inputPrice.sendKeys(productData.getPrice());
        waitForContentLoad(By.id("form_step1_qty_0_shortcut"));
        inputCount.sendKeys(String.valueOf(productData.getQty()));
        waitForContentLoad(By.id("form_step1_active"));
        checkbocksOnlineOffline.click();
        waitForContentVisible(By.xpath("//div[@class='growl-message']"));
        saveProduct.submit();
        waitForContentVisible(By.xpath("//div[@class='growl-message']"));
    }

}
